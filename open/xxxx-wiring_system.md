- Feature Name: `wiring_system`
- Start Date: 2021-03-04
- RFC Number: (leave this empty for now - the core developers will give you a number to put here when the RFC is submitted)
- Tracking Issue: (leave this empty - the core developers will give you a number to put here when the RFC is submitted)

# Summary
[summary]: #summary

Add a Turing complete wiring system to the game, where entities can have input and output components, and take actions based on signals, and emit signals for other entities to consume. Initially use it to add traps to dungeons, eventually make it player-mutable.

# Motivation
[motivation]: #motivation

1) Adding even a basic form of this (pressure plates as the only signal generators, crossbow turrets as the only signal consumers) would immediately allow adding more complexity to dungeons.
2) Dungeon complexity can be gradually and modularly expanded over time:
- By expanding the number of signal consumers, a pressure plate could trigger an explosion, a drop into a pit, retracting spikes, etc.
- By expanding the number of signal producers, the functionality of existing devices could be extended (e.g. by adding sine wave generators, a turret could shoot on a timer, and sweep back and forth).
- By adding logic gates (e.g. nand blocks), dungeon generation can make tamper-resistant traps: if the player severs a wire, the turret stops shooting at them, but a hatch opens under their feet.
- Logic gates would also allow pressure plate/lever puzzles to open dungeon doors.
3) Wiring synergistically effects other future planned features:
- Once quests are added, NPCs can hire adenturers to debug their town's automated ballista battery, or their harvesters.
- Once player owned carts/houses are added, this allows many more axes of customization (light switches, chutes for navigating a house, cart mounted ballistae).
- Once airships are added, they can make use of this to allow them to be crewed by a team (e.g. the xy-engine angle/power control, the z-levitation control, and the ballista angle/firing controls are crewed by different players/NPCs).

# Guide-level explanation
[guide-level-explanation]: #guide-level-explanation

1) Basic level (if only dungeon traps are implemented)

"If you see a pressure plate in a dungeon, stepping on it will probably cause arrows to shoot towards you."

2) Intermediate level (once traps have some complexity, and wire visibility/snipping exists for players).

Dungeons have a variety of traps in them, connected by wires.

You can make wiring visible briefly for you and your team with the "Detect wiring" skill. "Detect wiring" is unlockable through the `P` menu, and can be dragged to the hotbar to use like other weapon skills. "Detect wiring" costs $X stamina and makes wiring visible within some radius of you for $Y seconds.

If you see a pressure plate and a wire leading to a turret down the hallway, you can cut it with the "Cut wire" skill so that you won't trigger it.

If you see a pressure plate with a wire leading to a maze of wires inside a wall, and that maze leads to a visible turret, and also something above/below the room you're in, snipping the wrong wire may activate additional traps. Use the "Detect components" skill to see labels for the logic gates to figure out which wires (if any) are safe to cut.

3) Advanced level (once traps are deployable by players)

At a town's crafting station, you can make components for your own deployable traps {logic gates, turrets, etc} and preconfigure them for deployment with the "Deploy trap" skill.

(details TBD, what UI is convenient to implement probably depends on the iced refactor, it'd be nice to have a voxel editor come up on an inventory-like window that you can drag items/wires around in, and choose an origin for the configuration and relative positions of the components)

# Reference-level explanation
[reference-level-explanation]: #reference-level-explanation

Something like the following could be added to `common/src/comp/wiring.rs`:

```
struct WiringElement {
    inputs: HashMap<String, f32>
    outputs: HashMap<String, OutputFormula>,
    actions: Vec<WiringAction>,
}

enum OutputFormula {
    OnCollide { value: f32 },
    OnInteract { value: f32 },
    Constant { value: f32 },
    Input { name: String },
    SineWave { amplitude: f32, frequency: f32 },
    Logic { kind: LogicKind, left: Box<OutputFormula>, right: Box<OutputFormula> },
}

enum LogicKind {
    Min, // acts like Or
    Max, // acts like And
    Sub, // `|x| { 5.0 - x }` acts like Not, depending on reference voltages
}

struct WiringAction {
    formula: OutputFormula,
    theshold: f32, 
    kinds: Vec<WiringActionKind>,
}

enum WiringActionKind {
    SpawnProjectile { constr: Box<dyn Fn(&WiringElement) -> ProjectileConstructor> },
    SetBlockCollidability { coords: Vec3<i32>, collidable: bool },
}

```

Something like the following could be added to `common/sys/src/wiring.rs`: 

```
struct Wire {
    input_field: String,
    output_field: String,
    input_entity: Entity,
    output_entity: Entity,
}

struct WiresResource {
    wires: Vec<Wire>,
}
```

The following would be probably done using `EntityInfo` in `world/src/site/dungeon/mod.rs`:
- A pressure plate/lever would have no `inputs`/`actions`, and a single `OnCollide` or `OnInteract` output named "output".
- An arrow turret could have "angle", and "fire" inputs, have it's "angle" wired to an entity providing `SineWave` or `Constant` outputs, "fire" wired to the "output" of a pressure plate, and an threshold action for "fire" that spawns a projectile (reading the "angle" through the reference to itself in the callback).
- chute traps and puzzle doors (and megaman-style disappearing block platforming sections) can all be done with `SetBlockCollideability`

# Drawbacks
[drawbacks]: #drawbacks

## Performance
The above sketch is kind of allocation-happy, and will globally tick all circuits even if they're not near a player. I think this can probably be addressed by having the wiring system compile the `OutputFormula`e into a bytecode IR (so that the tree can be flat in a Vec/string lookups only have to be done when someone's editing a circuit), and using datalog-style deltas to only recalculate circuits whose inputs have changed.

## Complexity
Not all players may find editing logic circuits fun. Hopefully enough of the system adds richness to the world even for players that aren't editing circuits.

# Rationale and alternatives
[alternatives]: #alternatives

## Rationale

This system is intermediate in complexity and blends components from several systems in related games, see the prior art section.

## Alternatives

### Hardcoding dungeon traps/puzzles
Would be slower to iterate with, and more ad-hoc.

### Not having dungeon traps/puzzles at all
The status quo.

### Using `bool`s instead of `f32`s for circuit values / not having named inputs/outputs
Could be more performant with less optimization, wouldn't allow angles for turrets or airships

# Prior art
[prior-art]: #prior-art

In order by ascending complexity, the following games all have turing complete wiring systems.
- Minecraft
- Terraria
- Barotrauma
- Dwarf fortress
- Factorio

The system proposed in this RFC uses mostly Terraria-style components (dungeon traps), with some aspects of Barotrauma wiring (named floating point values instead of 1 bool per component).

## Minecraft
- [Redstone Circuits](https://minecraft.fandom.com/wiki/Redstone_Circuits)
- Each voxel has a bool associated with it, indicating whether it is powered or not
- Wires on adjacent blocks do OR, redstone torches do NOT, bigger gates are built out of these (not atomic)
- Has levers and pressure plates as inputs, powered minecarts and pistons as outputs

## Terraria
- [Terraria Wiring](https://terraria.gamepedia.com/Guide:Wiring)
- Graph structure with wires between components, the circuits function independently of distance, but you need 1 wire item per voxel
- [Atomic logic gate components](https://terraria.gamepedia.com/Guide:Wiring#Logic_Gates)
- Wires can be snipped to disable traps, this disconnects the circuit at the graph level, and removes an item at the voxel level
- Levers, switches, light sensors, $x-second timers as inputs
- A variety of traps as outputs (explosives, darts, disappearing blocks dropping boulders)
- Also pumps to interact with fluid dynamics, teleporters for fast travel

## Barotrauma
- [Barotrauma Wiring](https://barotrauma.gamepedia.com/Wiring)
- Graph structure, but unlike terraria, wires are atomic (snipping part of it puts the whole thing in your inventory as 1 wire)
- [Atomic arithmetic gate components](https://barotrauma.gamepedia.com/Wiring_Components)
- Distinction between signal wires and power wires
- Motion sensors, buttons, periscopes as signal sources
- Reactor as a power source, batteries and capacitors as power gates
- Doors, railguns, coilguns as signal sinks
- Engines, guns, fluid pumps, sonar, as power sinks (if the reactor dies, you can start sinking/rising very rapidly because you can't adjust the ballast tanks)

## Dwarf fortress
- [Dwarven computing](https://dwarffortresswiki.org/index.php/DF2014:Computing)
- Levers/pressure plates as inputs, hatches/doors/traps as outputs
- Doesn't manage wires for you, each lever only operates a single hatch/door/trap
- Non-atomic logic gates built using the flow of fluids/creatures/minecarts across pressure plates

## Factorio
- [Circuit network](https://wiki.factorio.com/Circuit_network)
- As the whole game is about automation, many components can be wired to the circuit network to actuate them
- Vector values on wires: each wire has a channel with integer values for items and pseudoitems, logic gates can be SIMD with the "each" pseudoitem channel descriptor
- [Atomic arithmetic gates](https://wiki.factorio.com/Arithmetic_combinator)
- `(3|copper plates> + 5|iron gears>) + (2|iron plates> + 6|iron gears>) = (3|copper plates> + 2|iron plates> + 11|iron gears>)`
- [Atomic boolean gates](https://wiki.factorio.com/Decider_combinator)
- `(3|copper plates> + 5|iron gears>) < (2|iron plates> + 6|iron gears>) = (0|copper plates> + 1|iron plates> + 1|iron gears>)`

# Unresolved questions
[unresolved]: #unresolved-questions

## What parts of the design do you expect to resolve through the RFC process before this gets merged?
Overall feedback on the structure.

## What parts of the design do you expect to resolve through the implementation of this feature before stabilization?
Balance, maybe? Not sure what stabilization means in this context, hopefully sequential parts of this from this get merged into master as soon as they're individually viable.

## What related issues do you consider out of scope for this RFC that could be addressed in the future independently of the solution that comes out of this RFC?
I expect the details of interactions with other systems (quests, airships, etc) to co-evolve with this feature, possibly with additional RFCs.
